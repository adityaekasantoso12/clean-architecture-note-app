package com.example.noteapp.di

import android.app.Application
import androidx.lifecycle.SavedStateHandle
import androidx.room.Room
import com.example.noteapp.feature_note.data.data_source.NoteDatabase
import com.example.noteapp.feature_note.data.repository.NoteRepositoryImpl
import com.example.noteapp.feature_note.domain.repository.NoteRepository
import com.example.noteapp.feature_note.domain.use_case.AddNote
import com.example.noteapp.feature_note.domain.use_case.DeleteNote
import com.example.noteapp.feature_note.domain.use_case.GetNote
import com.example.noteapp.feature_note.domain.use_case.GetNotes
import com.example.noteapp.feature_note.domain.use_case.NoteUseCases
import com.example.noteapp.feature_note.presentation.add_edit_note.AddEditNoteViewModel
import com.example.noteapp.feature_note.presentation.notes.NotesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { provideNoteDatabase(get()) }
    single { provideNoteRepository(get()) }
    single { provideNoteUseCases(get()) }
    viewModel { NotesViewModel(get()) }
    viewModel { (savedStateHandle: SavedStateHandle) ->
        AddEditNoteViewModel(get(), savedStateHandle)
    }
}

fun provideNoteDatabase(app: Application): NoteDatabase {
    return Room.databaseBuilder(
        app,
        NoteDatabase::class.java,
        NoteDatabase.DATABASE_NAME
    ).build()
}

fun provideNoteRepository(db: NoteDatabase): NoteRepository {
    return NoteRepositoryImpl(db.noteDao)
}

fun provideNoteUseCases(repository: NoteRepository): NoteUseCases {
    return NoteUseCases(
        getNotes = GetNotes(repository),
        deleteNote = DeleteNote(repository),
        addNote = AddNote(repository),
        getNote = GetNote(repository)
    )
}